package com.translator3.juan.juantranslator3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import net.gotev.speech.GoogleVoiceTypingDisabledException;
import net.gotev.speech.Logger;
import net.gotev.speech.Speech;
import net.gotev.speech.SpeechDelegate;
import net.gotev.speech.SpeechRecognitionNotAvailable;
import net.gotev.speech.ui.SpeechProgressView;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    EditText editText;
    private SpeechProgressView progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Speech.init(this, getPackageName());
        Logger.setLogLevel(Logger.LogLevel.DEBUG);

        editText = findViewById(R.id.editText);
        progress = findViewById(R.id.progress);
        Button button1 = findViewById(R.id.button1);
        Button button2 = findViewById(R.id.button2);

        // Speech to Text Button
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    // you must have android.permission.RECORD_AUDIO granted at this point
                    Speech.getInstance().startListening(progress, new SpeechDelegate() {
                        @Override
                        public void onStartOfSpeech() {
                            Log.i("speech", "speech recognition is now active");
                        }

                        @Override
                        public void onSpeechRmsChanged(float value) {
                            Log.d("speech", "rms is now: " + value);
                        }

                        @Override
                        public void onSpeechPartialResults(List<String> results) {
                            StringBuilder str = new StringBuilder();
                            for (String res : results) {
                                str.append(res).append(" ");
                            }

                            Log.i("speech", "partial result: " + str.toString().trim());
                        }

                        @Override
                        public void onSpeechResult(String result) {
                            Log.i("speech", "result: " + result);
                            //result is the input
                            editText.setText(result); //change the text to result
                        }

                    });
                } catch (SpeechRecognitionNotAvailable exc) {
                    Log.e("speech", "Speech recognition is not available on this device!");
                } catch (GoogleVoiceTypingDisabledException exc) {
                    Log.e("speech", "Google voice typing must be enabled!");
                }
            }
        });
        // Text To Speech Button
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String ttsoutput = editText.getText().toString();
                Speech.getInstance().say(ttsoutput);
            }
        });

    }



}
